\subsection{JCBB und CCDA im Vergleich}
% Vergleich algorithmischer Komplexität bzw. Laufzeit
% Vergleich spezifischer Vor- und Nachteile beider Algorithmen

Der JCBB Algorithmus konstruiert inkrementell einen Interpretationsbaum und sucht in diesem nach der Hypothese mit der größten Anzahl an 
gemeinsam kompatiblen Assoziationen mithilfe des JC Tests, siehe~\cref{equation_jct}.
Dadurch, dass im JC Test eine $m\times m$ Matrix invertiert werden muss (m entspricht der Anzahl der Beobachtungen), ergäbe sich im worst case für das 
Überprüfen der gemeinsamen Kompatibilität der Assoziationen einer Hypothese eine Laufzeit von $O(m^3)$, was durch die inkrementelle Evaluation 
auf $O(m^2)$ reduziert werden kann, siehe dazu~\cite{neira2001data}. Den größten Einfluss nimmt hier also schon die Anzahl an Beobachtungen $m$.
Da für alle Beobachtungen dieser Test durchgeführt werden muss, ergibt sich eine exponentielle Rechenkomplexität in der Anzahl an Beobachtungen.
\cref{img_jcbb_computational_cost} zeigt, dass der Branch-and-Bound Teil den dominierenden Schritt einnimmt und exponentiell wächst.

\begin{figure}[H]%
    \centering%
    \includegraphics[width=9cm]{images/jccb_cost_observations.png}%
    \caption{Komplexität der verschiedenen Teile von JCBB in Abhängigkeit der Anzahl an Beobachtungen~\citep{neira2001data}.}%
    \label{img_jcbb_computational_cost}%
\end{figure}%

Nach~\cite{neira2001data} zeigen Experimente einen empirischen Grenzwert von $O(1,53^m)$. Um eine vernünftige Echtzeitperformance zu garantieren, muss 
die Anzahl an Beobachtungen $m$ pro Hypothese limitiert werden, beispielsweise auf die zehn bis zwölf relevantesten.
\\
Der CCDA Algorithmus andererseits besteht aus folgenden Schritten: CG (Korrespondenzgraph) Konstruktion und maximum clique search. Die Komplexität der CG 
Konstruktion unterscheidet sich, wenn die Pose bekannt oder unbekannt ist.
~\cref{img_ccda_unknown} zeigt den grundlegenden Ablauf für die unbekannte Pose.

\begin{figure}[H]
    \centering
    \includegraphics[width=8cm]{images/ccda-unknown.png}
    \caption{Ablauf der CG Konstruktion~\citep[Kapitel 3.3.3]{bailey2002mobile}.}
    \label{img_ccda_unknown}
\end{figure}

\emph{MakeNodeSet} erstellt alle $mn$ relativen Constraints (wobei $m$ und $n$ der Mächtigkeit von $A$ bzw. $B$ entspricht), da alle Kombinationen aus beiden Mengen ohne 
absolute Constraints, d.h. unbekannte Pose, möglich sind. \emph{MakeRelativeConstraints} berechnet für beide Mengen für jedes Featurepaar das entsprechende 
Constraint. \emph{Sort($C_B$)} sortiert die zweite Menge aufsteigend nach den Distanzschätzungen. \emph{MatchConstraints} erstellt die Kanten des CG, 
indem die beiden Constraint-Mengen gematched werden. Für die Verbesserung der Effizienz wird nur jedes $C_{A_i}$ mit $C_{B_k} \in C_B$ überprüft, dessen
Distanzdifferenz zu dem Element in $B$ in einen festgelegte Schwellwert $\Delta d$ fällt. Diese Überprüfung erfolgt für jedes in diesen Bereich fallendes 
Element mit einem MA Gate zwischen den beiden Constraints. Die gesamte Komplexität für die CG Konstruktion mit unbekannter Pose beträgt daher aus den o.g. Schritten 
$O(mn + m^2 + n^2 + n^2 \log{n^2} + m^2 \log{n^2} + km^2)$, $k$ entspricht dabei empirisch $O(n)$.
Dies kann zu $O(n^2 \log{n^2} + km^2)$ vereinfacht werden~\citep[Kapitel 3.3.3]{bailey2002mobile}.
\\
% Ablauf Pseudocode unbekannt + Erklärung
Der Unterschied mit bekannter Pose besteht in der Anwesenheit von absoluten Constraints. Diese können entweder nach jedem relativen Constraint Match 
angewandt werden oder vor dem relativen Constraint Matching Prozess, hier angenommen, da es für eine bereits nicht-unsichere Pose des Roboters 
effizienter ist~\citep[Kapitel 3.3.4]{bailey2002mobile}. Dabei wird sich nur allein auf die absoluten Constraints verlassen.
Diese Version des Algorithmus unterscheidet sich u.a. darin, dass als Knoten im Graph nur noch Paare aus beiden Mengen in Frage kommen, deren absolute 
Constraints gültig sind. Dieser Schritt kostet auch $O(mn)$ Operationen. Die gemeinsame Kompatibilität jedes Knotenpaares $\{v_i, v_j\}$ muss nach Gültigkeit 
des injektiven Mapping Constraints mithilfe der Gültigkeit der relativen Constraints überprüft werden. Bei Erfolg wird eine entsprechende Kante zum CG 
hinzugefügt. Durch obige Schritte ergibt sich dann eine Komplexität von $O(mn + |V|)$, $|V|$ entspricht der Anzahl der Knoten im CG. Wenn die Unsicherheit 
der Pose klein genug ist und so die Größe von $V$ etwa $O(min\{m, n\})$ ist, ergibt sich eine Gesamtkomplexität von $O(mn)$ für die CG Konstruktion mit 
bekannter Pose.
\\
% Komplexität von maximum clique search
Zuletzt fehlt noch die Komplexität des maximum clique search Algorithmus, der in einem Graphen die größte maximale Clique sucht. Für unbekannte und 
bekannte Pose ist die durchschnittliche Komplexität in Abhängigkeit der beobachteten Features in beiden Fällen $O(m^2)$, d.h. quadratisch mit der Anzahl
der CG Knoten~\citep[Kapitel 3.3.8]{bailey2002mobile}. Für mehr Details zum Ablauf von maximum clique search, siehe~\citep[Kapitel 3.3.5]{bailey2002mobile}.
% nochmal JCBB vs CCDA kurz vergleichen, Vor- und Nachteile
Betrachtet man also rein die Komplexität beider Ansätze, hat CCDA gegenüber JCBB mit exponentieller Laufzeit die Nase vorne.
Letztendlich hat CCDA also mehr praktische Vorteile, was vor allem für Echtzeitperformance wichtig ist: Die Laufzeit ist geringer und vernünftige 
Datenassoziationsergebnisse können auch noch ohne absolute Constraints erzielt werden, was dann auftritt, wenn der Roboter \glqq verlorengegangen\grqq{} ist 
und seine Position neu bestimmen muss (sog. Relocalization).
