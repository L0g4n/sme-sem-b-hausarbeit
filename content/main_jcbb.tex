\subsection{Joint Compatibility Branch and Bound}

\newcommand{\observations}{
    $\mathbf{z} = [\mathbf{z}_1,\mathbf{...}, \mathbf{z}_n]^T$
}
\newcommand{\landmarksEstimates}{
    $\mathbf{\hat{x}} = [\mathbf{\hat{x}}_1, \mathbf{...}, \mathbf{\hat{x}}_m]^T$
}
\newcommand{\associations}{
    $E_k =  \{e_1, ..., e_j\}$
}
\newcommand{\jointObservation}{
    \begin{equation}
        \mathbf{z}_{E_k} = [\mathbf{z}_{e_1}, \mathbf{...}, \mathbf{z}_{e_j}]^T
    \end{equation}
}
\newcommand{\jointPredictedObservation}{
    \begin{equation}
        \hat{\mathbf{z}}_{E_k} = \mathbf{h}_{E_k}(\hat{\mathbf{x}}) = \begin{bmatrix}
            \mathbf{h}_{e_1}(\hat{\mathbf{x}}) \\
            \vdots \\
            \mathbf{h}_{e_j}(\hat{\mathbf{x}})
        \end{bmatrix}
    \end{equation}
}
\newcommand{\jointInnovation}{
    \begin{equation}
        v_{E_k} = \mathbf{z}_{E_k} - \hat{\mathbf{z}}_{E_k}
    \end{equation}
}
\newcommand{\jointInnovationCovariance}{ %
    \begin{equation}
        \mathbf{S}_{E_k} = \nabla \mathbf{h_x P \nabla h_{x}}^T + \mathbf{R}_{E_k}
    \end{equation}
}
\newcommand{\at}[2][]{#1|_{#2}}
\newcommand{\jointValidationGate}{
    \begin{equation}
        \label{equation_jct}
        M_{E_k} = v_{E_k}^T \mathbf{S}_{E_k}^{-1} v_{E_k} < \gamma_n
    \end{equation}
}

Der \emph{Joint Compatibility Branch and Bound} (JCBB) Algorithmus nach~\cite{neira2001data} besteht grundsätzlich aus zwei Schritten:
\begin{enumerate}
    \item Generierung von vorläufigen Hypothesen, d.h. Mengen von Datenassoziationen.
    \item Suche nach der größten Hypothese, deren Assoziationen gemeinsam kompatibel (\emph{jointly compatible}) sind.
\end{enumerate}
Wichtig ist dabei explizit die \emph{größte} Hypothese, da die Wahrscheinlichkeit, dass eine falsche Paarung mit allen anderen Paarungen gemeinsam kompatibel ist 
mit steigender Anzahl an Paarungen in einer Hypothese abnimmt.
Diese Suche im Lösungsraum erfolgt in einem sog. Interpretationsbaum (\emph{interpretation tree}).
Der erste Schritt ist also die Bestimmung der gemeinsamen Kompatibilität (JC) einer Hypothese. Diese wird 
durch die Berechnung eines einzigen gemeinsamen MA-Gates umgesetzt.
Gegeben ist dafür eine Menge von Beobachtungen \observations mit Kovarianz \textbf{R} und eine Menge von Landmarkenschätzungen \landmarksEstimates 
mit Kovarianz \textbf{P}. Die vorläufige Hypothese ist eine Menge von Assoziationen \associations, die zuvor bzgl. individueller Kompatibilität und injektiver 
Mapping Constraints ausgewählt wurde. 
Der Test für individuelle Kompatibilität eliminiert dabei statistisch unwahrscheinliche Assoziationen aus der Betrachtung (ähnlich zum Mahalanobis Abstand), siehe \citep[Kapitel 3.1.2]{cooper2005comparison}; 
Die injektiven Mapping Constraints legen fest, dass nicht zwei Beobachtungen in einer Hypothese der gleichen Landmarke zugeordnet werden, d.h.
die Hypothesen haben z.B. die Form $E_1 = \{(z_1, x_2), (z_2, x_3), (z_3, x_4)\}$.
Für die Assoziationspaarung $e_i$ bestehend aus $\mathbf{z_{e_i}}$ und $\mathbf{x_{e_i}}$ ist die gemeinsame 
Beobachtung gegeben durch \jointObservation mit Kovarianz $\mathbf{R}_{E_k}$ und die gemeinsame vorausgesagte Beobachtung 
ist wie folgt \jointPredictedObservation
% hier Messungsfunktion h erklären!
$\mathbf{h}$ stellt die Messungsfunktion dar: Liefert der Sensor z.B. \emph{range-bearing} Beobachtungen, d.h. eine Distanz $r$ mit 
Winkel $\theta$, die einer Landmarke mit $x$ und $y$ Koordinaten zugeordnet werden, ist die vorausgesagte Beobachtung anhand
dieses Beobachtungsmodells, das den euklidischen Abstand und Winkel liefert, durch
\begin{equation*}
    \mathbf{z} = \mathbf{h}(x) = \begin{bmatrix}
        \sqrt{x^2 + y^2} \\
        \arctan{\left(\frac{y}{x}\right)}
    \end{bmatrix}
\end{equation*}
gegeben~\citep[Kapitel 3.1.1]{bailey2002mobile}.
Davon ausgehend lassen sich gemeinsame Innovation und deren Kovarianz berechnen \jointInnovation \jointInnovationCovariance 
mit der Jacobi-Matrix $\nabla \mathbf{h_x} = \frac{\partial \mathbf{h}_{E_k}}{\partial \mathbf{x}}\at[\Big]{\hat{\mathbf{x}}}$, die die Linearisierung 
der Messungsfunktionen bzgl. der Landmarkenschätzungen durchführt. Die JC als Validation Gate ist 
schließlich \jointValidationGate wobei $n$ der Dimension des gemeinsamen Innovationsvektors gleicht~\citep[Kapitel 3.2.1]{bailey2002mobile}.

Nach der Bestimmung der gemeinsamen Kompatibilität muss jetzt noch die größte Hypothese im Lösungsraum ausgewählt werden. Das ist der sog.
\textit{Branch-and-Bound} Schritt des Algorithmus. Der Lösungsraum wird als Interpretationsbaum dargestellt. Jede Stufe stellt alle möglichen Assoziationen 
für eine bestimmte Beobachtung dar, und jeder absteigende Pfad von der Wurzel eine mögliche Menge von injektiven Assoziationen. 
In~\cref{img_jcbb_interpretation_tree} ist ein Beispiel für so einen Baum zu sehen.

\begin{figure}[H]
    \centering
    \includegraphics[width=9cm]{images/jcbb-interpretation-tree.png}
    \caption{Der Interpretationsbaum für ein Beispiel mit vier Landmarken und drei Beobachtungen~\citep[Kapitel 3.2.1]{bailey2002mobile}.%
        Das $\phi$ steht für Nichtzuweisungen. Die best gefundene Hypothese der markierten Suche ist $E_k = \{(z_1, x_3), (z_2, x_1), (z_3, x_4)\}$.%
    }
    \label{img_jcbb_interpretation_tree}
\end{figure}

Der Baum wird durch Tiefensuche inkrementell mit einer maximum likelihood Heuristik konstruiert, d.h. Knoten mit besserer JC werden zuerst untersucht. 
Bei Hinzufügen jedes Knotens in einer Stufe wird der JC Test durchgeführt; Sollte dieser fehlschlagen wird dieser und seine Kinder als ungültig markiert 
und muss nicht mehr untersucht werden. Außerdem wird ein absteigender Pfad nicht weiter durchsucht, wenn dieser nicht genug Knoten hat, um das derzeitige 
Maximum der Hypothese zu übertreffen. Sollte man mehrere Hypothesen der gleichen Größe erhalten, ist die Auswahl des maximum likelihood Ergebnisses aus 
dieser Menge eine gute Wahl~\citep[Kapitel 3.1.2]{bailey2002mobile}.
% TODO: Definition jacobi-matrix?
